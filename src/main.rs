mod parser;

fn main() {
    let url: &str = "https://moz.gov.ua";
    let url = url.parse::<hyper::Uri>().unwrap();

    if url.scheme_str() != Some("https") {
        println!("This example only works with 'https' URLs.");
        return;
    }

    parser::parse(url);
}