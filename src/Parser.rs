#[warn(unused_imports)]
#[warn(unused_mut)]
#[warn(non_snake_case)]
#[deny(warnings)]

extern crate hyper;

use hyper::{body::HttpBody as _, Client, Uri};
use tokio::io::{self, AsyncWriteExt as _};
use crate::parser::hyper::body::HttpBody;
use hyper_tls::HttpsConnector;
use scraper::Html;


type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

#[tokio::main]
pub async fn parse(url: Uri) {
    let res = get_page_body(url).await.unwrap();
}

async fn get_page_body(url: Uri) -> Result<()> {
    let https = HttpsConnector::new();

    let client = Client::builder()
        .build::<_, hyper::Body>(https);

    let mut response = client.get(url).await?;

    println!("Response: {}", response.status());

    while let Some(next) = response.data().await {
        let chunk = next?;
        io::stdout().write_all( &chunk).await?;
    }

    println!("\n\nDone!");

    return Ok(());
}